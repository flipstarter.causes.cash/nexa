Release Notes for Nexa 1.3.0.1
======================================================

Nexa version 1.3.0.1 is now available from:

  <https://gitlab.com/nexa/nexa/-/releases>

Please report bugs using the issue tracker at github:

  <https://gitlab.com/nexa/nexa/-/issues>

This is minor release of Nexa, for more information about Nexa see:

- https://nexa.org
- https://spec.nexa.org

Main changes in 1.3.0.1
-----------------------

This is list of the main changes that have been merged in this release:

- restructure rpc `consolidate` code to increase performance in case of very big wallet UTXOs set
- minor bug fixes

Commit details
--------------

- `e045b5435` Bump nexa version to 1.3.0.1 (Andrea Suisani)
- `af3736037` Add various assert lock helds where still needed (Peter Tschipper)
- `7e3b5be53` Remove complex trim functionality when flushing the coins cache (Peter Tschipper)
- `34261733b` Return an appropriate warning when wallet is unlocked and we try to run consolidate() (Peter Tschipper)
- `a8fd831ec` Fix compile warning in gettxpoolentry() (Peter TSchipper)
- `1d97feb54` Refactor cashlib (Griffith)
- `a03d7d874` Fix gettxmpoolentry by adding WRITELOCK (Peter Tschipper)
- `1c99bace0` Do not assign PreferencesRole to the Unlimited dialog widget (Andrea Suisani)
- `885a76f83` Replace Q_FOREACH where PAIRTYPE is used (Dagur Valberg Johannsson)
- `f04d8ddd3` Add a gitian stage to the CI (Andrea Suisani)
- `01aee411e` add missing disable-shared to static build command, needed for libgmp (Griffith)

Credits
-------

Thanks to everyone who directly contributed to this release:

- Andrea Suisani
- Dagur Valberg Johannsson
- Griffith
- Peter Tschipper
